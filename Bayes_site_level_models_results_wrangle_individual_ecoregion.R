library(brms)
library(tidyverse)
library(bayesplot)
library(BayesPostEst)
library(boot)
library(arm)
library(coda)
library(gridExtra)
library(grid)




cov_labels<-data.frame(variable=c("b_rs_temp_anomaly", "b_rs_elevation", "b_rs_chili",  "b_rs_slope", "b_rs_tpi", "b_rs_aspect"), 
                       cov=c("temp anomaly", "elevation", "CHILI", "slope", "tpi", "aspect"))







##### Ecoregions combined #####




#### Southern Rockies Model ####

mod_sr<-readRDS("mod_sr_landscape_032922.rds")


summary(mod_sr)


aspect<-conditional_effects(mod_sr, effects="rs_aspect")


png(filename="sr_aspect_conditional_effects_v2.png", width=4, height=3, res=700, units="in")
plot(aspect, plot=FALSE)[[1]]+
  theme_classic()+
  ylab("RoC*100")+
  xlab("Mean aspect (standardized)")
dev.off()


mod_sr_mcmc<-as.array(mod_sr)


cov_labels<-data.frame(variable=c("b_rs_elevation", "b_rs_chili",  "b_rs_slope", "b_rs_tpi", "b_rs_aspect"), 
                       cov=c( "elevation", "CHILI", "slope", "tpi", "aspect"))


sr_site_labels<-data.frame(variable=c("r_site.id[Bison.Lake_20616,Intercept]", "r_site.id[Black.Mountain.Lake_16222,Intercept]",   
                                      "r_site.id[Como.Lake_3056,Intercept]", "r_site.id[Copley.Lake_506,Intercept]", "r_site.id[Cottonwood.Pass.Pond_514,Intercept]",    
                                      "r_site.id[East.Glacier.Lake_20922,Intercept]", "r_site.id[Echo.Lake_22400,Intercept]", "r_site.id[Emerald.Lake_41614,Intercept]",          
                                      "r_site.id[Hunters.Lake_20304,Intercept]", "r_site.id[Lake.Emma_15106,Intercept]",             
                                      "r_site.id[Little.Windy.Hill.Pond_46611,Intercept]", "r_site.id[Long.Lake_22969,Intercept]",             
                                      "r_site.id[Molas.Lake_1761,Intercept]",  "r_site.id[Redrock.Lake_2049,Intercept]",           
                                      "r_site.id[Sky.Pond_16224,Intercept]", "r_site.id[Splains.Lake_2588,Intercept]",          
                                      "r_site.id[Twin.Lakes_2880,Intercept]"), 
                           site=c("Bison.Lake_20616", "Black.Mountain.Lake_16222",   
                                  "Como.Lake_3056", "Copley.Lake_506", "Cottonwood.Pass.Pond_514",    
                                  "East.Glacier.Lake_20922", "Echo.Lake_22400", "Emerald.Lake_41614",          
                                  "Hunters.Lake_20304", "Lake.Emma_15106",             
                                  "Little.Windy.Hill.Pond_46611", "Long.Lake_22969",             
                                  "Molas.Lake_1761",  "Redrock.Lake_2049",           
                                  "Sky.Pond_16224", "Splains.Lake_2588",          
                                  "Twin.Lakes_2880"))

png(filename="fixed_effects_plot_mod_sr_v2.png", width=4, height=6, res=700, units="in")
mcmc_areas(mod_sr_mcmc, pars=c("b_rs_elevation", "b_rs_chili",  "b_rs_slope", "b_rs_tpi", "b_rs_aspect"), prob=.95)+
  scale_y_discrete(labels=as.character(cov_labels$cov))
  
dev.off()



png(filename="re_intercept_plot_mod_sr_v2.png", width=8, height=10, res=700, units="in")
mcmc_areas(mod_sr_mcmc, pars=c("r_site.id[Bison.Lake_20616,Intercept]", "r_site.id[Black.Mountain.Lake_16222,Intercept]",   
                               "r_site.id[Como.Lake_3056,Intercept]", "r_site.id[Copley.Lake_506,Intercept]", "r_site.id[Cottonwood.Pass.Pond_514,Intercept]",    
                               "r_site.id[East.Glacier.Lake_20922,Intercept]", "r_site.id[Echo.Lake_22400,Intercept]", "r_site.id[Emerald.Lake_41614,Intercept]",          
                               "r_site.id[Hunters.Lake_20304,Intercept]", "r_site.id[Lake.Emma_15106,Intercept]",             
                               "r_site.id[Little.Windy.Hill.Pond_46611,Intercept]", "r_site.id[Long.Lake_22969,Intercept]",             
                               "r_site.id[Molas.Lake_1761,Intercept]",  "r_site.id[Redrock.Lake_2049,Intercept]",           
                               "r_site.id[Sky.Pond_16224,Intercept]", "r_site.id[Splains.Lake_2588,Intercept]",          
                               "r_site.id[Twin.Lakes_2880,Intercept]"), prob=.95)+
  scale_y_discrete(labels=as.character(sr_site_labels$site))

dev.off()



#### Middle Rockies Model ####
mod_mr<-readRDS("mod_mr_landscape_032922.rds")


summary(mod_mr)

conditional_effects(mod_mr)


mod_mr_mcmc<-as.array(mod_mr)


cov_labels<-data.frame(variable=c( "b_rs_elevation", "b_rs_chili",  "b_rs_slope", "b_rs_tpi", "b_rs_aspect"), 
                       cov=c("elevation", "CHILI", "slope", "tpi", "aspect"))


mr_site_labels<-data.frame(variable=c("r_site.id[Blacktail.Pond_279,Intercept]", "r_site.id[Cub.Creek.Pond_533,Intercept]", "r_site.id[Cub.Lake_534,Intercept]",   
                                      "r_site.id[Divide.Lake_701,Intercept]", "r_site.id[Emerald.Lake_796,Intercept]", "r_site.id[Hedrick.Pond_1030,Intercept]",  
                                      "r_site.id[Lower.Paintrock.Lake_46645,Intercept]",  "r_site.id[Mariposa.Lake_1707,Intercept]", "r_site.id[Rainbow.Lake_46659,Intercept]",          
                                      "r_site.id[Rapid.Lake_2026,Intercept]","r_site.id[Whitebark.Moraine.Pond_42559,Intercept]"),
                           site=c("Blacktail.Pond_279", "Cub.Creek.Pond_533", "Cub.Lake_534",   
                                  "Divide.Lake_701", "Emerald.Lake_796", "Hedrick.Pond_1030",  
                                  "[Lower.Paintrock.Lake_46645",  "Mariposa.Lake_1707", "Rainbow.Lake_46659",          
                                  "Rapid.Lake_2026","Whitebark.Moraine.Pond_42559"))


png(filename="fixed_effects_plot_mod_mr_v2.png", width=4, height=6, res=700, units="in")
mcmc_areas(mod_mr, pars=c( "b_rs_elevation", "b_rs_chili",  "b_rs_slope", "b_rs_tpi", "b_rs_aspect"), prob=.95)+
  scale_y_discrete(labels=as.character(cov_labels$cov))
dev.off()



png(filename="re_intercept_plot_mod_mr_v2.png", width=8, height=10, res=700, units="in")
mcmc_areas(mod_mr, pars=c("r_site.id[Blacktail.Pond_279,Intercept]", "r_site.id[Cub.Creek.Pond_533,Intercept]", "r_site.id[Cub.Lake_534,Intercept]",   
                               "r_site.id[Divide.Lake_701,Intercept]", "r_site.id[Emerald.Lake_796,Intercept]", "r_site.id[Hedrick.Pond_1030,Intercept]",  
                               "r_site.id[Lower.Paintrock.Lake_46645,Intercept]",  "r_site.id[Mariposa.Lake_1707,Intercept]", "r_site.id[Rainbow.Lake_46659,Intercept]",          
                               "r_site.id[Rapid.Lake_2026,Intercept]","r_site.id[Whitebark.Moraine.Pond_42559,Intercept]"), prob=.95)+
  scale_y_discrete(labels=as.character(mr_site_labels$site))

dev.off()
