## ---------------------------
##
## Script name: 00-trace21ka-covariate-extraction
##
## Purpose of script: This script was developed to extract ecoregion-level covariate data from the Trace21Ka raster products generated using the PaleoView application to describe paleoclimatic conditions in Western North America from 21,000 bp to present. 
##
## Author: Patrick T. Freeman, Conservation Science Partners, Inc. 
##
## Date Last Modified: 2022-03-22
##
## Email: patrick@csp-inc.org
##
## ---------------------------
##
## Notes: 

## This script uses rasters summarizing Trace21Ka climate data produced through the PaleoView application describing the 80yr average climate conditions at 100 yr timesteps to produce climate histories for Level III Ecoregions 17 (Middle Rockies) and 21 (Southern Rockies). See below for how these products were generated and see associated PaleoView documentation for interacting with the application and downloading gridded climate data. Analyses can be reproduced once climate rasters are placed in the data/spatial-data directory 

## To capture information on the medium- to long-term paleoclimatic conditions at the scale of ecoregions and sites, we used outputs from the TRaCE21ka experiment (Liu et al. 2009, 2014, Otto-Bliesner et al. 2014). TRaCE21ka used the Community Climate System Model ver. 3 (CCSM3) to model paleoclimate spanning the period from 22 000 BP to the present (1989 AD). The PaleoView data application (Fordham, et al. 2017) was used to generate spatially explicit estimates of mean, minimum, and maximum temperature and mean precipitation rasters at a spatial resolution of 2.5 x 2.5° (latitude/longitude). PaleoView utilizes monthly-mean climate model data from the TRaCE21ka simulation experiment. The raw climate rasters were downloaded to a local machine prior to producing the climate output rasters produced by PaleoView. 

## In the PaleoView application, climate data can be averaged over set interval windows down to a minimum resolution of 10 years using the “Interval size” option. The time step between each estimate of climatic conditions is set using the “Interval step” setting. Therefore, if ‘Interval size’ was set to 30 years  and ‘Interval step’ was set to 50 years for the time period spanning 15,000 BP to 14,800 BP, PaleoView would return 30-year average climate estimates for the period between 15,000 BP (climate conditions calculated between 15,015 - 14,986 BP) to 14,800 BP (climate conditions between 14,815 - 14,786 BP) at 50 year time steps and a total of four individual rasters. 

## In order to approximately match the temporal scale at which rate-of-change analyses were conducted while operating within the constraints of the options provided by PaleoView, we generated global climate rasters with estimates of mean, minimum, and maximum annual temperature and mean annual precipitation using an 80-year interval size and an interval step of 100 years spanning the full period from 21,000 to 0 BP. 

## ---------------------------

#### Load necessary libraries ####
library(raster)
library(sf)
library(tidyverse)


### Read in the PaleoView-generated raster stacks ####
meanTemp <- brick("data/spatial-data/Mean_Temperature_Annual_21000BP-0BP_step100_size80.nc")

minTemp <- brick("data/spatial-data/Minimum_Temperature_Annual_21000BP-0BP_step100_size80.nc")

maxTemp <- brick("data/spatial-data/Maximum_Temperature_Annual_21000BP-0BP_step100_size80.nc")

meanPrecip <- brick("data/spatial-data/Mean_Precipitation_Annual_21000BP-0BP_step100_size80.nc")


#### Load ecoregion boundary shapefiles and transform to WGS 84 projection ####
ecoregions <- st_read("data/spatial-data/Ecoregions_17_21.shp") %>% 
  st_transform(., crs=4326) %>% 
  st_as_sf()


#### Reproject to match raster projection ####
ecoregions.tr <- st_transform(ecoregions, crs=crs(climate.data))


#### Split into middle and southern rockies ####
middle.rockies <- ecoregions.tr %>% 
  dplyr::filter(EcoName == "Middle Rockies")

southern.rockies <- ecoregions.tr %>% 
  dplyr::filter(EcoName == "Southern Rockies")


### Crop rasters to reduce size/decrease processing time ####
meanTemp_10crop <- crop(meanTemp, as_Spatial(ecoregions.tr))
minTemp_10crop <- crop(minTemp, as_Spatial(ecoregions.tr))
maxTemp_10crop <- crop(maxTemp, as_Spatial(ecoregions.tr))
meanPrecip_10crop <- crop(meanPrecip, as_Spatial(ecoregions.tr))

mean_min_max_stack_21000_crop <- crop(mean_min_max_stack_21000, as_Spatial(ecoregions.tr))

#### Extract temperature values using spatially-weighted averaging that takes into account how much area of a pixel is covered by the ecoregion boundary ####

meanTemp.middle <-  raster::extract(meanTemp_10crop, as_Spatial(middle.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="mean_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Middle Rockies") %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

meanTemp_southern <-  raster::extract(meanTemp_10crop, as_Spatial(southern.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="mean_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Southern Rockies") %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

meanTemp_ecoregions <- bind_rows(meanTemp.middle, meanTemp_southern)


minTemp.middle <-  raster::extract(minTemp_10crop, as_Spatial(middle.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="min_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Middle Rockies") %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

minTemp_southern <-  raster::extract(minTemp_10crop, as_Spatial(southern.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="min_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Southern Rockies") %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

minTemp_ecoregions <- bind_rows(minTemp.middle, minTemp_southern)



maxTemp.middle <-  raster::extract(maxTemp_10crop, as_Spatial(middle.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="max_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Middle Rockies",
                IntervalSize = 80) %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

maxTemp_southern <-  raster::extract(maxTemp_10crop, as_Spatial(southern.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="max_temp.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Southern Rockies",
                IntervalSize=80) %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

maxTemp_ecoregions <- bind_rows(maxTemp.middle, maxTemp_southern)

meanPrecip.middle <-  raster::extract(meanPrecip_10crop, as_Spatial(middle.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="Precip.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Middle Rockies",
                IntervalSize = 80) %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)

meanPrecip_southern <-  raster::extract(meanPrecip_10crop, as_Spatial(southern.rockies), fun=mean, df=T, weights=T) %>% 
  pivot_longer(., cols=c(2:last_col()), names_to="raster.extracted", values_to="Precip.ecoregion") %>% 
  dplyr::mutate(raster.extracted = str_remove(raster.extracted, "X."),
                EcoName = "Southern Rockies",
                IntervalSize=80) %>% 
  dplyr::mutate_at("raster.extracted", as.numeric)


meanPrecip_ecoregions <- bind_rows(meanPrecip.middle, meanPrecip_southern)


### Bind all climate covariates together ####
clim_covariates <- left_join(meanTemp_ecoregions, minTemp_ecoregions, by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  left_join(., maxTemp_ecoregions,by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  left_join(., meanPrecip_ecoregions, by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  dplyr::relocate(., mean_temp.ecoregion, .after = max_temp.ecoregion) %>% 
  group_by(raster.extracted,EcoName) %>% 
  pivot_longer(., min_temp.ecoregion:Precip.ecoregion, names_to="Trace_ClimateVar", values_to = "Value") %>% 
  dplyr::rename(year = raster.extracted) %>% 
  dplyr::mutate(year = year*-1)


#### Convert climate covariates from long to wide format for modeling input ####
clim_covariates_wide <- left_join(meanTemp_ecoregions, minTemp_ecoregions, by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  left_join(., maxTemp_ecoregions,by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  left_join(., meanPrecip_ecoregions, by=c("ID", "raster.extracted", "EcoName", "IntervalSize")) %>% 
  dplyr::relocate(., mean_temp.ecoregion, .after = max_temp.ecoregion) %>% 
  group_by(raster.extracted,EcoName) %>% 
  dplyr::rename(year = raster.extracted) %>% 
  dplyr::mutate(year = year*-1)


#### Write Database To File ####
#write_csv(clim_covariates_wide, "Paleotransformation_Update/00_Data/03_RoC_Data/Ecoregion_17_21_TraceClimateCovariates_wide_20220207.csv")

