# Risk of ecological transformation across the US West and Pinyon woodlands

This project investigated how climate change over the last 21,000 years, which was characterized by significant warming, influenced vegetation in the Southern and Middle Rockies. We found that rapid vegetation change was initiated across these landscapes once a 2 ℃ temperature increase was realized and again recently with reduced rainfall. Southwesterly slopes in the Southern Rockies were prone to rapid change, otherwise landscape features didn’t have a strong effect. We also examined vegetation transformations (e.g., sagebrush steppe switches to a lodgepole pine forest) and identified between one and four vegetation transformations at each site, for a total of 60 transformations, over half of which occurred rapidly. This work provides a novel understanding of vegetation change that integrates climate change and landscape context, and helps to anticipate when (once our climate warms by 2 ℃ (before 2050)) and where (southwesterly slopes in the Southern Rockies) rapid vegetation change and transformation will be likely.

For a fully-detailed report on this project that provides context for data acquisition, processing, and analysis please visit this link: [DOI TO FINAL REPORT]

# This Repository

This repository contains the scripts necessary to reproduce the workflows and analyses that led to the conclusions outlined above. Data inputs for running scripts can be obtained from [here]. For clarification and/or inquiries about workflows please email Dr. Shelley Crausbay at shelley[at]csp-inc.org

# Part 1: Pollen and geochronology datasets

We leveraged existing open source paleorecords of vegetation change across the Middle and Southern Rockies, along with open source paleoclimate data, to better understand rapid ecological change and ecological transformation.

We used the neotoma package ([Goring et al. 2015](https://www.openquaternary.com/article/10.5334/oq.ab/)) to acquire pollen records (a proxy for vegetation) and associated geochronologies from 29 sites across our two focal ecoregions (Level III Ecoregions 17 and 21). We used a Bayesian age-depth modeling framework implemented via the R package ‘rbacon’ ([Blaauw et al. 2019]()) to determine the age of sampled strata. We used the ‘rbaconizer’ scripts  developed by [Wang et al. (2019)]() and the geochronology data acquired from the neotoma database to run the age-depth models for our sites in bulk. Or, when our consistent set of parameters resulted in a poor model fit for an individual site, we ran individual models and adjusted the model parameters to improve model fit.

* For clarity and simplicity,[we provide a directory of all final Bacon model outputs](https://drive.google.com/open?id=1yik_oBIXQ8d9ZclejaRFGMku-QZUc9LD&authuser=patrick%40csp-inc.org&usp=drive_fs) that were produced for the final study sites in Level III Ecoregions 17 and 21.
* We also provide a [database that identifies, for each site, the model parameters that were used in the age-depth model](https://docs.google.com/spreadsheets/d/1MFqNrDqgJSy87OHXXBe2GOJL5rAxc-TpnsogF9GWBUY?authuser=patrick%40csp-inc.org&usp=drive_fs) and whether the site was run individually or through a bulk approach.

Final site-level pollen community datasets used for subsequent analyses of ecological transformations are provided in the directory: [&#39;eco_17_21_focal_sites&#39;](https://drive.google.com/open?id=16Jqvx-rCms4S0SgxemurFSrx0zSLw0sU&authuser=patrick%40csp-inc.org&usp=drive_fs)

# Part 2: Scripts for analyzing and summarizing transformations in pollen assemblages

## Cluster Analysis

**Cluster_analysis_percentages_eco1721_final.R**

* Script to run cluster analyses for focal sites in Level III ecoregions 17 and 21
  This script loops through each site and does the following:
  * Converts all data to percentages
  * Creates a dissimilarity matrix using the ‘bray-curtis’ method using the function ‘vegdist’ from the package ‘vegan’
  * Runs a cluster analysis on the dissimilarity matrix using the method ‘coniss’ with the function ‘chclust’ from the ‘rioja’ package
  * Creates a dataframe containing the ‘heights’ which describes the magnitude of ecological change.
  * Runs a ‘broken stick’ model on each cluster analysis, which determines the number of distinct ecological groups
  * Also creates a database of number of groups for each dataset
  * Exports a stratigraphic plot with clusters for visualization
  * Creates a database of ‘degree of change’ to compare amongst sites
  * Creates a database of summary stats to compare amongst sites

**inputs:**

* .csv files in [eco_17_21_focal_sites](https://drive.google.com/open?id=16Jqvx-rCms4S0SgxemurFSrx0zSLw0sU&authuser=patrick%40csp-inc.org&usp=drive_fs)

**outputs:**

* [breakpoint_files_eco_17_21](https://drive.google.com/open?id=1l2Wyr1KUis7hBim3eAf_bOa51s5oJFzL&authuser=patrick%40csp-inc.org&usp=drive_fs)

## Part 3: Scripts for analyzing the effects of climate and landscape features on the ecological rate-of-change

## Ecological Rate-of-change Data Preparation and Analysis

The final script for running rate-of-change analyses requires several inputs including standardized community datasets, ages derived from an age-depth model, and associated age uncertainty derived from 1000 randomly sampled posterior draws from the age-depth model.

Prior to analysis, we harmonized taxa across sites, removed unidentified taxa, and standardized to an equal 95 grains for each sampled stratum using the ‘rrarefy’ function in the R package ‘vegan’. A directory with the standardized community data are provided below. For questions about data pre-processing please contact the project PI.

## Rate-of-change analysis

Rate-of-Change.R

* This script performs the rate-of-change analysis. We conducted rate-of-change analysis for each site individually with the R package ‘R-Ratepol’ (Mottl et al. 2021). We first smoothed the standardized pollen assemblage data with an age-weighted average. We then created working units of 500-year time bins and calculated the rate of change (Bray-Curtis dissimilarity divided by time) between working units using a moving window approach that shifts every 100 years, incorporates 1000 draws of age uncertainty, and is repeated 10,000 times. This script also detects and validates significant peak points in the rate-of-change record with the non-linear trend method built into ‘R-Ratepol’. 

**Inputs:**

* [CommunityData_standardized]() - directory containing the standardized/rarefied community data such that only strata with greater than or equal to 95 total pollen grains were retained
* [SiteAges_Standardized]() - directory containing the ages associated with each stratum of the standardized communities
* [AgeUncertainty_Bacon_1000Draws](https://drive.google.com/open?id=19pN51COKjBWv6aGbTO8Sn5ln_g1RcrXr&authuser=patrick%40csp-inc.org&usp=drive_fs) - directory containing the 1000 random posterior draws from the Bacon age-depth models that capture uncertainty around the age estimates for each stratum

**Outputs:**

* The script above produces individual, site-level rate-of-change dataframes that were then merged together by S.D. Crausbay for further analyses. These compiled records include:
  * [full-roc-records.csv](https://drive.google.com/open?id=1DzpNV2lqsSu2uZklaDJskhYxl9LVcgBA&authuser=patrick%40csp-inc.org&usp=drive_fs) -- which includes a full record of the rate-of-change across the site's time series
  * [roc-peaks-annotated.csv](https://drive.google.com/open?id=1E8GBDjwPMAEl_zbZy9SeQmAU3cycaWTL&authuser=patrick%40csp-inc.org&usp=drive_fs) -- which includes only those parts of site records that were highlighted as "peak" rates of change that have been annotated with additional metadata

## Climate covariate data acquisition and processing for Level III Ecoregions 17 and 21

Climate covariates were aggregated over the entire area of Level III Ecoregions 17 and 21 and at the 'site' level, where underlying climate covariates were extracted for each individual site.

Scripts that reproduce the climate covariate extraction at the 'landscape' level are:

00-stableclim-covariate-extraction.R

* This script was developed to extract ecoregion-level covariate data from the StableClim raster products describing trends and variability in temperature and precipitation since 21,000  bp available at: https://adelaide.figshare.com/articles/dataset/StableClim/12197976. To perform this raster extraction, first download the StableClim regression netCDF rasters from the link in the header into your chosen directory (see script for suggested file structure). Analyses were performed on products from StableClim_V1.0.1
* **Inputs:**
  * [Ecoregion shapefile](https://drive.google.com/open?id=1EmiSqZ_yH6zkGvllKtDBp-AV7TAu2W4D&authuser=patrick%40csp-inc.org&usp=drive_fs)
  * StableClim regression rasters [see notes in script for downloading rasters]
  * [StableClim_SamplingFrame.csv](https://drive.google.com/open?id=1FToaBvmBzpnbTU1ZeU5nX8ZC7gj408pI&authuser=patrick%40csp-inc.org&usp=drive_fs) [a necessary file to set the rasters for extraction]
* **Outputs:**
  * [eco17_21_stableclim-vars_100yrbins.csv](https://drive.google.com/open?id=1ECEQEugZE4ze7anhMQF6GTHlaHEV8ERB&authuser=patrick%40csp-inc.org&usp=drive_fs)

00-trace21ka-covariate-extraction.R

* This script was developed to extract ecoregion-level covariate data from the Trace21Ka raster products generated using the PaleoView application to describe paleoclimatic conditions in Western North America from 21,000 bp to present. Importantly, read the notes in the script about how to generate the climate rasters from the Trace21Ka experiment using the the PaleoView application.
* **Inputs:**
  * Trace21Ka rasters [read the notes in the script about how to generate the climate rasters from the Trace21Ka experiment using the the PaleoView application]
  * [Ecoregion shapefiles]()
* **Outputs:**
  * [Ecoregion_17_21_TraceClimateCovariates_wide_20220207.csv](https://drive.google.com/open?id=1Fu8rd7yp-2RKvMo0w6kcl8b-TIsUIUXo&authuser=patrick%40csp-inc.org&usp=drive_fs) [these data were manually transformed to anomalies relative to 21,000 bp by S.D. Crausbay before subsequent analyses]

Scripts that reproduce the climate covariate extraction at the 'site' level are:

19. 00-trace21ka-covariate-sitelevel-extraction.R
    * This script was developed to extract site-level covariate data from the Trace21Ka raster products generated using the PaleoView application to describe paleoclimatic conditions in Western North America from 21,000 bp to present. Importantly, read the notes in the script about how to generate the climate rasters from the Trace21Ka experiment using the the PaleoView application.
    * **Inputs:**
      * Trace21Ka rasters [read the notes in the script about how to generate the climate rasters from the Trace21Ka experiment using the the PaleoView application]
      * [Eco_17_21_sites_shp]()
    * **Outputs:**
      * [AllSites_Trace21ka_Climate_21000bp-0bp_2022-02-14.csv]() [these data were not ultimately used in final Bayesian models]

## Landscape covariate data

To obtain site-level estimates of topographic landscape covariates we used a custom Google Earth Engine script that does the following:

* For each site in our analysis, we digitized a polygon around the perimeter of the lake, and created a 1000m buffer which represents the relevant pollen source area. Using Google Earth Engine, we calculated the mean and standard deviation within the 1000m buffer for several landscape characteristics that we hypothesized to play a role in vegetation dynamics. These included aspect, continuous heat-insolation load index (CHILI), slope, multi-scale topographic position index (mTPI), and elevation. We derived slope and aspect metrics from the USGS National Elevation dataset (⅓ Arc-second), CHILI and mTPI metrics from the ERGo google earth engine asset (Theobald et al. 2015, 30m resolution), and elevation from the NASA SRTM Digital Elevation map (30m resolution). Custom Google Earth Engine scripts used to extract topographic data can be provided upon reasonable request from the project PI.

## Boosted regression tree models

Prior to running boosted regression tree models we calculated the proportion of sites in each ecoregion with peak rates of change at 100-year time steps. The response variable is included in the input .csv files used for each of the three final boosted regression tree models along with relevant climate covariates.

Boosted Regression Trees.R

* This script is used to run three independent models that assess the relationship between past climate change and rapid landscape-scale ecological change.
* **Inputs**

  * [PaleoData.csv](https://drive.google.com/open?id=1E8EZzeiiJmTHoyDBAQfwf70reKKzJ6WI&authuser=patrick%40csp-inc.org&usp=drive_fs)
  * [PaleoData_7500.csv](https://drive.google.com/open?id=1pXGHVIjgLtb7xfzmZ1brsowNSJ__fjE-&authuser=patrick%40csp-inc.org&usp=drive_fs)
* **Outputs:**

  * [paleoBRT_0-21000yr.rds](https://drive.google.com/open?id=1kwi1xtmQe0-hdJoAzCzKNAwAHcxeLpnY&authuser=patrick%40csp-inc.org&usp=drive_fs)
  * [paleoBRT_0-7500.rds](https://drive.google.com/open?id=1veQgRqHPbS2e0LVRsXtik3PnCsJ0H3B0&authuser=patrick%40csp-inc.org&usp=drive_fs)
  * [paleoBRT_7500-21000.rds](https://drive.google.com/open?id=1D2g5dQ14cz8Jql4t9GCJYKk11FGntLSy&authuser=patrick%40csp-inc.org&usp=drive_fs)


## Bayesian site-level models

Bayesian_site_level_models_individual_ecoregions.R

* This script is used to run a hierarchical model that explores the effects of various landscape-level covariates on the RoC. It includes a random intercept of site to get site-level responses.
* Inputs:
  * [eco_17_21_data_and_covariates_for_bayes_analysis.rds](https://drive.google.com/open?id=1DiKs9CZy3fWqg664walpZXfl5LUW48kO&authuser=patrick%40csp-inc.org&usp=drive_fs) - this input dataframe (provided as an R data object) contains all covariates included in final models. Individual scripts for covariate wrangling can be provided by the project PI upon request.
* Outputs
  * [mod_sr_landscape_032222.rds]()
  * [mod_mr_landscape_032222.rds](https://drive.google.com/open?id=1DrlglDjyDIYE4kv14EIrnxlPOGzzEO2d&authuser=patrick%40csp-inc.org&usp=drive_fs)
